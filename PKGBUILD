# Maintainer: Bernhard Landauer <bernhard@manjaro.org>
# Archlinux credits:
# Based on the file created for Arch Linux by:
# Massimiliano Torromeo <massimiliano.torromeo@gmail.com>
# Bob Fanger <bfanger(at)gmail>
# Filip <fila pruda com>, Det <nimetonmaili(at)gmail>

_linuxprefix=linux518-rt
_extramodules=extramodules-5.18-rt-MANJARO
pkgname=$_linuxprefix-r8168
_pkgname=r8168
pkgver=8.050.03
pkgrel=4
pkgdesc="A kernel module for Realtek 8168 network cards"
url="http://www.realtek.com.tw"
license=("GPL")
arch=('x86_64')
depends=('glibc' "$_linuxprefix")
makedepends=("$_linuxprefix-headers")
provides=("$_pkgname=$pkgver")
replaces=('linux510-rt-r8168')
groups=("$_linuxprefix-extramodules")
source=("https://github.com/mtorromeo/r8168/archive/refs/tags/$pkgver.tar.gz"
        'linux518.patch')
sha256sums=('76f9e7c26a8ade7b01dd34060f5b17d74387f15e9b6baa6dbba8c43634a31ce6'
            'd8d542770e504775600f686d03412a37cc32489872be7aeb388b5b08d9806096')

install=$_pkgname.install

prepare() {
    cd "$_pkgname-$pkgver"
    patch -p1 -i ../linux518.patch
}

build() {
    _kernver="$(cat /usr/lib/modules/$_extramodules/version || true)"

    cd "$_pkgname-$pkgver"

    # avoid using the Makefile directly -- it doesn't understand
    # any kernel but the current.

    make -C /usr/lib/modules/$_kernver/build \
      M="$srcdir/$_pkgname-$pkgver/src" \
      EXTRA_CFLAGS="-DCONFIG_R8168_NAPI -DCONFIG_R8168_VLAN -DCONFIG_ASPM -DENABLE_S5WOL -DENABLE_EEE" \
      modules
}

package() {
    cd "$_pkgname-$pkgver/src"
    install -D -m644 $_pkgname.ko "$pkgdir/usr/lib/modules/$_extramodules/$_pkgname.ko"

    # set the kernel we've built for inside the install script
    sed -i -e "s/EXTRAMODULES=.*/EXTRAMODULES=${_extramodules}/g" "${startdir}/${_pkgname}.install"

    find "$pkgdir" -name '*.ko' -exec gzip -9 {} \;
}
